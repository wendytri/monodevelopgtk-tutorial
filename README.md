### Summary ###
This repository is dedicated to lightly explore MonoDevelop IDE using GTK# Project with Ubuntu.

### Setup ###

```
#!ubuntu

1. Install FlatPak for Ubuntu using terminal as followed:
sudo add-apt-repository ppa:alexlarsson/flatpak
sudo apt update
sudo apt install flatpak

2. Install Mono's mcs as the compiler:
sudo apt install mono-mcs

3. Install MonoDevelop via FlatPak:
flatpak install --user --from https://download.mono-project.com/repo/monodevelop.flatpakref

4. Install Gtk# 2
sudo apt-get gtk-sharp2
```

### Tutorial ###
Classic tutorial 'Hello World' for beginner to familiarize with Gtk# Programming.
Basic guidance can be found here: http://www.mono-project.com/docs/gui/gtksharp/beginners-guide/
The tutorial will consist of:

```
#!C#

1. Writing a Hello World programs with Gtk#

2. Code compilation using mcs command
   mcs filename.cs -pkg:gtk-sharp-2.0
   
3. Ensure that every Gtk# application must import the Gtk namespace
   using Gtk;
   
4. Ensure that GTK is initialised and runnable in every Gtk# application
   Application.Init()
   Application.Run()

5. Explore and modify Hello World program freely to gain experience
```